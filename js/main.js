const jokesContainer = document.querySelector('.jokes-block')
const btnGetJoke = document.querySelector('.btn-get-joke')
const inputCategory = document.getElementById('categ-search')
const inputSearchPhrase = document.getElementById('phrase-search')

inputCategory.addEventListener('change', function () {
    const categ = document.querySelector('.categories')
    categ.classList.add('active')
    const categType = document.querySelectorAll('.category-type')
    categType.forEach(elem => elem.addEventListener('click', function () {
        this.parentElement.querySelector('.current').classList.remove('current')
        elem.classList.add('current')
    })
    )
})

inputSearchPhrase.addEventListener('change', function () {
    const searchPhrase = document.querySelector('.search-text')
    searchPhrase.classList.add('active')
})



btnGetJoke.addEventListener('click', function (e) {
    e.preventDefault()
    const input = document.querySelectorAll('.search-type')
    console.log(input)
    input.forEach(item => {
        // console.log(this)
        if (item.id === 'random-search') {
            // $.getJSON('https://api.chucknorris.io/jokes/random', createJocke({ categories, id, value, updated_at }))
            $.getJSON('https://api.chucknorris.io/jokes/random', function ({ url, categories, id, value, updated_at }) {
                const now = new Date()
                const updated = new Date(updated_at)
                const update = Math.floor((now - updated) / 3600000)
                jokesContainer.innerHTML = `<img class ="add-to-favourite" src="./img/addFavourite.png">
                <img class="text-ico" src="./img/textIco.png">
                <a href=${url} class="id-link">ID: ${id}
                <img src="./img/link.png"></a>
        <p class="joke-text">${value}</p>
        <p class="update-info">Last update: ${update} hours ago</p>
        <p class="category-type joke-type">${categories}</p>`

            //     const addToFavourite = document.querySelector('.add-to-favourite')
            //     addToFavourite.addEventListener('click', function () {
            //         this.setAttribute('src', './img/addFavouriteActive.png')
            //         const favouriteJokesContainer = document.querySelector('.favourite-jokes-block')
            //         favouriteJokesContainer.insertAdjacentHTML('beforeend', `<img class ="add-to-favourite" src="./img/addFavourite.png">
            //         <img class="text-ico" src="./img/textIco.png">
            //         <a href=${url} class="id-link">ID: ${id}
            //         <img src="./img/link.png"></a>
            // <p class="joke-text">${value}</p>
            // <p class="update-info">Last update: ${update} hours ago</p>
            // <p class="category-type joke-type">${categories}</p>`)
            //     })
            })

        }
        else if (item.id === 'categ-search') {
            const activeCategory = document.querySelector('.category-type.current').textContent
            // console.log(activeCategory)
            $.getJSON('https://api.chucknorris.io/jokes/random?category=' + activeCategory, function ({ url, categories, id, value, updated_at }) {
                // console.log(categories)    
                const now = new Date()
                const updated = new Date(updated_at)
                const update = Math.floor((now - updated) / 3600000)
                jokesContainer.innerHTML = `<img class ="add-to-favourite" src="./img/addFavourite.png">
                <img class="text-ico" src="./img/textIco.png">
                <a href=${url} class="id-link">ID: ${id}
                <img src="./img/link.png"></a>
        <p class="joke-text">${value}</p>
        <p class="update-info">Last update: ${update} hours ago</p>
        <p class="category-type joke-type">${categories}</p>`
            })
        }
        else {
            const searchPhrase = document.querySelector('.search-text').value
            // console.log(searchPhrase)


            $.getJSON('https://api.chucknorris.io/jokes/search?query=' + searchPhrase, function ({ url, categories, id, value, updated_at }) {
                const now = new Date()
                const updated = new Date(updated_at)
                const update = Math.floor((now - updated) / 3600000)
                jokesContainer.innerHTML = `<img class ="add-to-favourite" src="./img/addFavourite.png">
                <img class="text-ico" src="./img/textIco.png">
                <a href=${url} class="id-link">ID: ${id}
                <img src="./img/link.png"></a>
        <p class="joke-text">${value}</p>
        <p class="update-info">Last update: ${update} hours ago</p>
        <p class="category-type joke-type">${categories}</p>`
            })
        }
    })


})

// function createJocke(categories, id, value, updated_at) {
//     const now = new Date()
//     const updated = new Date(updated_at)
//     const update = Math.floor((now - updated) / 3600000)
//     jokesContainer.innerHTML = `<p class="id-text">ID: ${id}</p>
// <p class="joke-text">${value}</p>
// <p class="update-info">Last update: ${update} hours ago</p>
// <p class="category-type joke-type">${categories}</p>`
// }


